# -*- coding: utf-8 -*-

from .parser import parse


def main():
    with open('./ThriftTest.thrift') as f:
        data = f.read()
    parse(data)


if __name__ == '__main__':
    main()
