from ply import lex, yacc
from .ast import Include, Annotation, Namespace, MapConst, ListType, \
    SetType, MapType, PrimitiveType, Type, Const, Document, TypeDef, \
    EnumItem, Enum, Field, Struct, Function, Service


class SyntaxError(Exception):
    pass


reserved = {
    'include': 'INCLUDE',
    'cpp_include': 'CPP_INCLUDE',
    'namespace': 'NAMESPACE',
    'struct': 'STRUCT',
    'union': 'UNION',
    'enum': 'ENUM',
    'typedef': 'TYPEDEF',
    'exception': 'EXCEPTION',
    'service': 'SERVICE',
    'extends': 'EXTENDS',
    'bool': 'BOOL',
    'byte': 'BYTE',
    'i8': 'I8',
    'i16': 'I16',
    'i32': 'I32',
    'i64': 'I64',
    'double': 'DOUBLE',
    'string': 'STRING',
    'binary': 'BINARY',
    'map': 'MAP',
    'set': 'SET',
    'list': 'LIST',
    'slist': 'SLIST',
    'cpp_type': 'CPP_TYPE',
    'void': 'VOID',
    'oneway': 'ONEWAY',
    'required': 'REQUIRED',
    'optional': 'OPTIONAL',
    'throws': 'THROWS',
    'const': 'CONST',
}

tokens = list(reserved.values()) + ['INT_CONSTANT']


# Global field ident value.
class _IdentValue:
    def __init__(self):
        self._value = -1

    @property
    def value(self):
        v = self._value
        self._value -= 1
        return v

    def reset(self):
        self._value = -1

field_ident_value = _IdentValue()
del _IdentValue


def _extract_pos(t):
    if hasattr(t, 'lexspan'):
        lexspan = t.lexspan
    else:
        lexspan = getattr(t, 'lexpos', 0), getattr(t, 'endlexpos', 0)
    return getattr(t, 'lineno', 0), getattr(t, 'column', 0), lexspan


def _prepare_token(t):
    t.endlexpos = t.lexpos + len(t.value)
    lexer = t.lexer
    linepos = getattr(lexer, '_tparse_linepos', 0)
    t.column = t.lexpos - linepos + 1


def define_token(sth, value=None):
    """
    @define_token
    def t_TOKEN(t):
        r''
        return t

    define_token('TOKEN', '')
    """
    if callable(sth):
        name = sth.__name__
        doc = sth.__doc__
    else:
        name = sth
        doc = value

        def sth(t):
            return t

    def func(t):
        _prepare_token(t)
        return sth(t)

    func.__name__ = 't_' + name
    func.__doc__ = doc

    globals()['t_' + name] = func
    if name.isupper():
        tokens.append(name)
    return sth


t_ignore = ' \t'
define_token('STAR', '\*')
define_token('GT', '>'),
define_token('LT', '<'),
define_token('OPEN_CURLY', '{')
define_token('CLOSE_CURLY', '}')
define_token('OPEN_SQUARE', '\[')
define_token('CLOSE_SQUARE', '\]')
define_token('OPEN_PAREN', '\(')
define_token('CLOSE_PAREN', '\)')
define_token('EQUAL', '=')
define_token('COLON', ':')
define_token('LITERAL', r'''("[^"]*")|('[^']*')''')
define_token('COMMA', ',')
define_token('SEMICOLON', ';')


@define_token
def COMMENT(t):
    r'(/\*(.|\n)*?\*/)|(//.*)|(\#.*)'
    t.lexer.lineno += len(t.value.split('\n')) - 1


@define_token
def newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)
    t.lexer._tparse_linepos = t.lexer.lexpos


@define_token
def HEX_CONSTANT(t):
    r'[-+]?0(x|X)[0-9A-Fa-f]+'
    t.value = int(t.value, 16)
    t.type = 'INT_CONSTANT'
    return t


@define_token
def DOUBLE_CONSTANT(t):
    r'[-+]?[0-9]*\.?[0-9]+((E|e)[-+]?[0-9]+)?'
    if set(t.value).isdisjoint(('.Ee')):
        t.type = 'INT_CONSTANT'
        t.value = int(t.value)
    else:
        t.value = float(t.value)
    return t


@define_token
def IDENTIFIER(t):
    r'[a-zA-Z][a-zA-Z_0-9]*(\.[a-zA-Z][a-zA-Z_0-9]*)*'
    t.type = reserved.get(t.value, 'IDENTIFIER')
    return t


def _process_repeat(p):
    # collection : collection element
    #            | empty
    if p[0] is None:
        p[0] = []
    if len(p) >= 3:
        if p[1]:
            p[0].extend(p[1])
        p[0].append(p[2])


def p_empty(p):
    'empty :'


def p_separator(p):
    """separator : COMMA
                 | SEMICOLON
                 | empty
    """


def p_document(p):
    'document : headers definitions'
    p[0] = Document(p[1], p[2], *_extract_pos(p.slice[1]))


def p_headers(p):
    """headers : empty
               | headers header
    """
    _process_repeat(p)


def p_header(p):
    """header : include
              | namespace
    """
    p[0] = p[1]


def p_include(p):
    """include : INCLUDE LITERAL
               | CPP_INCLUDE LITERAL
    """
    p[0] = Include(p[1], p[2], *_extract_pos(p.slice[2]))


def p_namespace(p):
    """namespace : NAMESPACE IDENTIFIER IDENTIFIER annotations
                 | NAMESPACE STAR IDENTIFIER
    """
    annotations = p[4] if len(p) == 5 else []
    p[0] = Namespace(p[2], p[3], annotations, *_extract_pos(p.slice[2]))


def p_annotations(p):
    """annotations : OPEN_PAREN annotation_items CLOSE_PAREN
                   | empty
    """
    if len(p) != 4:
        p[0] = []
    else:
        p[0] = p[2]


def p_annotation_items(p):
    """annotation_items : annotation_items annotation
                        | empty
    """
    _process_repeat(p)


def p_annotation(p):
    'annotation : IDENTIFIER annotation_value separator'
    p[0] = Annotation(p[1], p[2], *_extract_pos(p.slice[1]))


def p_annotation_value(p):
    """annotation_value : EQUAL LITERAL
                        | empty
    """
    p[0] = p[2] if len(p) == 3 else None


def p_definitions(p):
    """definitions : definitions definition
                   | empty
    """
    _process_repeat(p)


def p_definition(p):
    """definition : const
                  | service
                  | typedef
                  | enum
                  | struct
    """
    p[0] = p[1]


def p_service(p):
    'service : SERVICE IDENTIFIER extends OPEN_CURLY functions CLOSE_CURLY annotations'  # noqa
    p[0] = Service(p[2], p[3], p[5], p[7], *_extract_pos(p.slice[2]))


def p_extends(p):
    """extends : EXTENDS IDENTIFIER
               | empty
    """
    p[0] = p[2] if len(p) == 3 else None


def p_functions(p):
    """functions : functions function
                 | empty
    """
    _process_repeat(p)


def p_function(p):
    'function : oneway function_type IDENTIFIER OPEN_PAREN fields CLOSE_PAREN throws annotations separator'  # noqa
    p[0] = Function(p[1], p[2], p[3], p[5], p[7], p[8],
                    *_extract_pos(p.slice[3]))


def p_oneway(p):
    """oneway : ONEWAY
              | empty
    """
    p[0] = len(p) == 2


def p_function_type(p):
    """function_type : type
                     | VOID
    """
    p[0] = p[1]


def p_throws(p):
    """throws : THROWS OPEN_PAREN fields CLOSE_PAREN
              | empty
    """
    p[0] = p[3] if len(p) == 5 else None


def p_struct(p):
    'struct : struct_head IDENTIFIER OPEN_CURLY fields CLOSE_CURLY annotations'
    p[0] = Struct(p[1], p[2], p[4], p[6], *_extract_pos(p.slice[2]))


def p_struct_head(p):
    """struct_head : STRUCT
                   | UNION
                   | EXCEPTION
    """
    p[0] = p[1]


def p_fields(p):
    """fields : fields field
              | empty
    """
    _process_repeat(p)
    if len(p) != 3:
        field_ident_value.reset()


def p_field(p):
    'field : field_id field_req type IDENTIFIER field_value annotations separator'  # noqa
    p[0] = Field(p[1], p[2], p[3], p[4], p[5], p[6], *_extract_pos(p.slice[4]))


def p_field_id(p):
    """field_id : INT_CONSTANT COLON
                | empty
    """
    if len(p) != 3:
        p[0] = field_ident_value.value
    else:
        p[0] = p[1]


def p_field_req(p):
    """field_req : REQUIRED
                 | OPTIONAL
                 | empty
    """
    p[0] = p[1] if len(p) == 2 else None


def p_field_value(p):
    """field_value : EQUAL const_value
                   | empty
    """
    p[0] = p[2] if len(p) == 3 else None


def p_typedef(p):
    'typedef : TYPEDEF type IDENTIFIER annotations separator'
    p[0] = TypeDef(p[2], p[3], p[4], *_extract_pos(p.slice[3]))


def p_enum(p):
    'enum : ENUM IDENTIFIER OPEN_CURLY enum_defs CLOSE_CURLY annotations'
    p[0] = Enum(p[2], p[4], p[6], *_extract_pos(p.slice[2]))


def p_enum_defs(p):
    """enum_defs : enum_defs enum_def
                 | empty
    """
    _process_repeat(p)


def p_enum_def(p):
    'enum_def : enum_value annotations separator'
    name, value, pos = p[1]
    p[0] = EnumItem(name, value, p[2], *pos)


def p_enum_value(p):
    """enum_value : IDENTIFIER EQUAL INT_CONSTANT
                  | IDENTIFIER
    """
    if len(p) == 2:
        p[0] = p[1], None
    else:
        p[0] = p[1], p[3]
    p[0] += _extract_pos(p.slice[1]),


def p_const(p):
    'const : CONST type IDENTIFIER EQUAL const_value separator'
    p[0] = Const(p[3], p[2], p[5], *_extract_pos(p.slice[3]))


def p_const_value(p):
    """const_value : INT_CONSTANT
                   | DOUBLE_CONSTANT
                   | LITERAL
                   | IDENTIFIER
                   | list_const
                   | map_const
    """
    p[0] = p[1]


def p_list_const(p):
    'list_const : OPEN_SQUARE list_const_values CLOSE_SQUARE'
    p[0] = p[2]


def p_list_const_values(p):
    """list_const_values : list_const_values const_value separator
                         | empty
    """
    _process_repeat(p)


def p_map_const(p):
    'map_const : OPEN_CURLY map_const_values CLOSE_CURLY'
    p[0] = p[2]


def p_map_const_values(p):
    """map_const_values : map_const_values const_value COLON const_value separator
                        | empty
    """
    if p[0] is None:
        p[0] = []
    if len(p) > 3:
        if p[1]:
            p[0].extend(p[1])
        p[0].append(MapConst(p[2], p[4], _extract_pos(p.slice[2])))


def p_type(p):
    """type : IDENTIFIER
            | base_type
            | container_type
    """
    if len(p[1]) == 2:
        item, annotations = p[1]
        pos = _extract_pos(item)
    else:
        item, annotations = p[1], None
        pos = _extract_pos(p.slice[1])
    p[0] = Type(item, annotations, *pos)


def p_base_type(p):
    'base_type : simple_base_type annotations'
    p[0] = p[1], p[2]


def p_primitive_type(p):
    """simple_base_type : STRING
                        | BINARY
                        | SLIST
                        | BOOL
                        | I8
                        | I16
                        | I32
                        | I64
                        | DOUBLE
    """
    p[0] = PrimitiveType(p[1], *_extract_pos(p.slice[1]))


def p_container_type(p):
    'container_type : simple_container_type annotations'
    p[0] = p[1], p[2]


def p_simple_container_type(p):
    """simple_container_type : map_type
                             | set_type
                             | list_type
    """
    p[0] = p[1]


def p_cpp_type(p):
    """cpp_type : CPP_TYPE LITERAL
                | empty
    """
    p[0] = p[2] if len(p) == 3 else None


def p_map_type(p):
    'map_type : MAP cpp_type LT type COMMA type GT'
    p[0] = MapType(p[4], p[6], p[2], *_extract_pos(p.slice[1]))


def p_set_type(p):
    'set_type : SET cpp_type LT type GT'
    p[0] = SetType(p[4], p[2], *_extract_pos(p.slice[1]))


def p_list_type(p):
    'list_type : LIST LT type GT cpp_type'
    p[0] = ListType(p[3], p[5], *_extract_pos(p.slice[1]))


def p_error(p):
    raise SyntaxError('Invalid token: {!r} at {}:{}'.format(
        p.value, p.lineno, p.column))


def parse(data):
    lexer = lex.lex()
    lexer.input(data)
    parser = yacc.yacc(start='document')
    return parser.parse(input=data, lexer=lexer)
