# -*- coding: utf-8 -*-


class DataMeta(type):
    @staticmethod
    def _find_fields(bases, attrs):
        if '__fields__' in attrs:
            return attrs['__fields__']

        for base in bases:
            fields = getattr(base, '__fields__', None)
            if fields is not None:
                return fields
        raise RuntimeError('No "__fields__" defined.')

    def __new__(mcls, name, bases, attrs):
        if name != 'Data':
            fields = mcls._find_fields(bases, attrs)
            attrs['__slots__'] = tuple(fields) + (
                'lineno', 'column', 'lexspan')
        cls = type.__new__(mcls, name, bases, attrs)
        if name == 'Data':
            return cls
        slots = cls.__slots__
        bodys = [
            '    self.{0} = {0}'.format(slot) for slot in slots
        ]
        code = 'def __init__(self, {}):\n{}'.format(
            ','.join(slots), '\n'.join(bodys))
        ns = {}
        exec(code, None, ns)
        cls.__init__ = ns['__init__']
        return cls


def __repr__(self):
    classname = self.__class__.__name__
    if 'name' in self.__slots__:
        name_info = ' {}'.format(self.name)
    else:
        name_info = ''
    return '<{}{} at {}:{}>'.format(classname, name_info, self.lineno,
                                    self.column)

Data = DataMeta('Data', (object,), {'__slots__': (), '__repr__': __repr__})
del __repr__


class Document(Data):
    __fields__ = ('headers', 'definitions')


class Include(Data):
    __fields__ = ('include_type', 'module')


class Annotation(Data):
    __fields__ = ('name', 'value')


class Namespace(Data):
    __fields__ = ('name', 'identity', 'annotations')


class MapConst(Data):
    __fields__ = ('key', 'value')


class Const(Data):
    __fields__ = ('name', 'value_type', 'value')


class ListType(Data):
    __fields__ = ('element_type', 'cpp_type')


class SetType(ListType):
    pass


class MapType(Data):
    __fields__ = ('key_type', 'value_type', 'cpp_type')


class PrimitiveType(Data):
    __fields__ = ('name',)


class Type(Data):
    __fields__ = ('item', 'annotations')


class TypeDef(Data):
    __fields__ = ('type', 'name', 'annotations')


class EnumItem(Data):
    __fields__ = ('name', 'value', 'annotations')


class Enum(Data):
    __fields__ = ('name', 'items', 'annotations')


class Field(Data):
    __fields__ = ('seq', 'requiredness', 'field_type', 'name', 'field_value',
                  'annotations')


class Struct(Data):
    __fields__ = ('struct_type', 'name', 'fields', 'annotations')


class Function(Data):
    __fields__ = ('is_oneway', 'return_type', 'name', 'arguments', 'throws',
                  'annotations')


class Service(Data):
    __fields__ = ('name', 'extends', 'functions', 'annotations')
